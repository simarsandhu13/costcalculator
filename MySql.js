var express = require("express");
var mysql = require("mysql");
const app = express();

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "admin",
  database: "costcalculator",
});

app.get("/activationtypetable", (req, res) => {
  con.connect(function (err) {
    if (err) throw err;
    con.query(
      "SELECT * FROM activationtypetable;",
      function (err, result, fields) {
        if (err) throw err;
        res.send(result);
      }
    );
  });
});

app.get("/audienceintereststable", (req, res) => {
  con.connect(function (err) {
    if (err) throw err;
    con.query(
      "SELECT * FROM audienceintereststable;",
      function (err, result, fields) {
        if (err) throw err;
        res.send(result);
      }
    );
  });
});

app.get("/influencersintereststable", (req, res) => {
  con.connect(function (err) {
    if (err) throw err;
    con.query(
      "SELECT * FROM influencersintereststable;",
      function (err, result, fields) {
        if (err) throw err;
        res.send(result);
      }
    );
  });
});

app.get("/influencerstable", (req, res) => {
  con.connect(function (err) {
    if (err) throw err;
    con.query(
      "SELECT * FROM influencerstable;",
      function (err, result, fields) {
        if (err) throw err;
        res.send(result);
      }
    );
  });
});

app.get("/inputstable", (req, res) => {
  con.connect(function (err) {
    if (err) throw err;
    con.query("SELECT * FROM inputstable;", function (err, result, fields) {
      if (err) throw err;
      res.send(result);
    });
  });
});

app.get("/interestssettable", (req, res) => {
  con.connect(function (err) {
    if (err) throw err;
    con.query(
      "SELECT * FROM interestssettable;",
      function (err, result, fields) {
        if (err) throw err;
        res.send(result);
      }
    );
  });
});

app.get("/platformstable", (req, res) => {
  con.connect(function (err) {
    if (err) throw err;
    con.query("SELECT * FROM platformstable;", function (err, result, fields) {
      if (err) throw err;
      res.send(result);
    });
  });
});
app.get("/tiertable", (req, res) => {
  con.connect(function (err) {
    if (err) throw err;
    con.query("SELECT * FROM tiertable;", function (err, result, fields) {
      if (err) throw err;
      res.send(result);
    });
  });
});
app.get("/tierverticalplatformtable", (req, res) => {
  con.connect(function (err) {
    if (err) throw err;
    con.query(
      "SELECT * FROM tierverticalplatformtable;",
      function (err, result, fields) {
        if (err) throw err;
        res.send(result);
      }
    );
  });
});

app.get("/usageinstancestable", (req, res) => {
  con.connect(function (err) {
    if (err) throw err;
    con.query(
      "SELECT * FROM usageinstancestable;",
      function (err, result, fields) {
        if (err) throw err;
        res.send(result);
      }
    );
  });
});

app.get("/verticalstable", (req, res) => {
  con.connect(function (err) {
    if (err) throw err;
    con.query("SELECT * FROM verticalstable;", function (err, result, fields) {
      if (err) throw err;
      res.send(result);
    });
  });
});

app.listen(3000, () => {});
