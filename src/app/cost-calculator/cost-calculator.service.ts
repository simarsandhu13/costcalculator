import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CostCalculatorService {
  getVerticalTypes(): Observable<any> {
    // return this.http.get('http://localhost:3000/verticalstable');
    return this.http.get(
      'https://costcalculators-default-rtdb.firebaseio.com/verticaltypestable.json'
    );
  }

  getTierTable(): Observable<any> {
    return this.http.get(
      'https://costcalculators-default-rtdb.firebaseio.com/tiertable.json'
    );
  }

  getInfluencerTypes(): Observable<any> {
    return this.http.get(
      'https://costcalculators-default-rtdb.firebaseio.com/influencertypestable.json'
    );
  }

  getActivationTypes(): Observable<any> {
    return this.http.get(
      'https://costcalculators-default-rtdb.firebaseio.com/activationtypestable.json'
    );
  }

  constructor(private http: HttpClient) {}
}
