import { Component, OnInit } from '@angular/core';
import { CostCalculatorService } from './cost-calculator.service';

@Component({
  selector: 'app-cost-calculator',
  templateUrl: './cost-calculator.component.html',
  styleUrls: ['./cost-calculator.component.css'],
})
export class CostCalculatorComponent implements OnInit {
  TierTable: {
    TierID: number;
    LowerBound: number;
    UpperBound: number;
    TierDescription: string;
    TierRate: number;
  }[] = [];

  verticalTypes: {
    VerticalID: number;
    VerticalName: string;
    VerticalDescription: string;
    VerticalRate: number;
  }[] = [];

  influencerTypes: {
    InfluencerID: number;
    InfluencerName: string;
    TierID: number;
  }[] = [];

  activationTypes: {
    PlatformID: number;
    ActivationLabel: string;
    ActivationDescription: string;
    BasePrice: number;
  }[] = [];

  NumberOfCompaigns: {
    influencerTypeSelection: number;
    activationTypeSelection: number;
    TotalInfluencers: number;
    TotalActivations: number;
  }[] = [
    {
      influencerTypeSelection: 0,
      activationTypeSelection: 0,
      TotalInfluencers: 0,
      TotalActivations: 0,
    },
  ];

  verticalTypeSelection: number | undefined;
  tierRate: number = 0;
  totalCost: number = 0;

  constructor(private costCalculatorService: CostCalculatorService) {}

  ngOnInit(): void {
    this.getVerticalTypes();
    this.getTierTable();
    this.getInfluencersTypes();
    this.getActivationTypes();
  }

  calculateCost() {
    this.totalCost = 0;
    this.NumberOfCompaigns.forEach((element) => {
      if (
        this.verticalTypeSelection &&
        element.influencerTypeSelection &&
        element.activationTypeSelection
      ) {
        var item = this.TierTable.find(
          (item) => item.TierID === element.influencerTypeSelection
        );
        if (item) {
          this.tierRate = item?.TierID;
        }
        this.totalCost =
          this.totalCost +
          this.verticalTypeSelection *
            this.tierRate *
            element.activationTypeSelection;
      }
    });
  }

  AddRow(bool: boolean) {
    if (bool) {
      this.NumberOfCompaigns.push({
        influencerTypeSelection: 0,
        activationTypeSelection: 0,
        TotalInfluencers: 0,
        TotalActivations: 0,
      });
    } else {
      this.NumberOfCompaigns.pop();
    }
  }

  getVerticalTypes() {
    this.costCalculatorService.getVerticalTypes().subscribe((Params) => {
      if (Params) {
        this.verticalTypes = Params;
      }
    });
  }

  getTierTable() {
    this.costCalculatorService.getTierTable().subscribe((Params) => {
      if (Params) {
        this.TierTable = Params;
      }
    });
  }

  getInfluencersTypes() {
    this.costCalculatorService.getInfluencerTypes().subscribe((Params) => {
      if (Params) {
        this.influencerTypes = Params;
      }
    });
  }

  getActivationTypes() {
    this.costCalculatorService.getActivationTypes().subscribe((Params) => {
      if (Params) {
        this.activationTypes = Params;
      }
    });
  }
}
